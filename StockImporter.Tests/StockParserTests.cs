﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace StockImporter.Tests
{
    [TestClass]
    public class StockParserTests
    {
        [TestMethod]
        public void ShouldReturnMaterialsFromInput()
        {
            var input = new string[] 
            {
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10"
            };
            var parser = new StockParser();
            IEnumerable<Material> materials = parser.Parse(input);            

            Assert.AreEqual(2, materials.Count());

            Assert.AreEqual("Cherry Hardwood Arched Door - PS", materials.ElementAt(0).Name);
            Assert.AreEqual("Cherry Hardwood Arched Door - PS", materials.ElementAt(1).Name);

            Assert.AreEqual("COM-100001", materials.ElementAt(0).ID);
            Assert.AreEqual("COM-100001", materials.ElementAt(1).ID);

            Assert.AreEqual("WH-A", materials.ElementAt(0).Stock);
            Assert.AreEqual("WH-B", materials.ElementAt(1).Stock);

            Assert.AreEqual(5, materials.ElementAt(0).Amount);
            Assert.AreEqual(10, materials.ElementAt(1).Amount);
        }

        [TestMethod]
        public void ShouldReturnMaterialsFromManyInputs()
        {
            var input = new string[]
            {
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10",
                "Maple Dovetail Drawerbox;COM-124047;WH-A,15",
                "Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2",
                "Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11",
                "Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black;CB0115-CASSRC;WH-C,13|WH-B,5",
                "Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3MCherry-10mm;WH-A,10|WH-B,1",
                "Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10",
                "MDF, CARB2, 1 1/8;COM-101734;WH-C,8"
            };

            var parser = new StockParser();
            IEnumerable<Material> materials = parser.Parse(input);

            Assert.AreEqual(14, materials.Count());
        }
    }
}
