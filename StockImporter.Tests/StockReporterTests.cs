﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace StockImporter.Tests
{
    [TestClass]
    public class StockReporterTests
    {
        [TestMethod]
        public void ShouldPrepareReportFromMaterials()
        {
            var materials = new Material[] 
            {
                new Material { Name = "Cherry Hardwood Arched Door - PS", ID = "COM-100001", Stock = "WH-A", Amount = 5 },
                new Material { Name = "Cherry Hardwood Arched Door - PS", ID = "COM-100001", Stock = "WH-B", Amount = 10 },
                new Material { Name = "Maple Dovetail Drawerbox", ID = "COM-124047", Stock = "WH-A", Amount = 15 },
                new Material { Name = "Generic Wire Pull", ID = "COM-123906c", Stock = "WH-A", Amount = 10 },
                new Material { Name = "Generic Wire Pull", ID = "COM-123906c", Stock = "WH-B", Amount = 6 },
                new Material { Name = "Generic Wire Pull", ID = "COM-123906c", Stock = "WH-C", Amount = 2 },
                new Material { Name = "Yankee Hardware 110 Deg. Hinge", ID = "COM-123908", Stock = "WH-A", Amount = 10 },
                new Material { Name = "Yankee Hardware 110 Deg. Hinge", ID = "COM-123908", Stock = "WH-B", Amount = 11 },
                new Material { Name = "Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black", ID = "CB0115-CASSRC", Stock = "WH-C", Amount = 13 },
                new Material { Name = "Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black", ID = "CB0115-CASSRC", Stock = "WH-B", Amount = 5 },
                new Material { Name = "Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back", ID = "3MCherry-10mm", Stock = "WH-A", Amount = 10 },
                new Material { Name = "Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back", ID = "3MCherry-10mm", Stock = "WH-B", Amount = 1 },
                new Material { Name = "Veneer - Cherry Rotary 1 FSC", ID = "COM-123823", Stock = "WH-C", Amount = 10 },
                new Material { Name = "MDF, CARB2, 1 1/8", ID = "COM-101734", Stock = "WH-C", Amount = 8 }


            };
            var reporter = new StockReporter();


            IEnumerable<ReportRow> reportRows = reporter.GetReport(materials);

            Assert.AreEqual(3, reportRows.Count());
            
            Assert.AreEqual(50, reportRows.ElementAt(0).Total);
            Assert.AreEqual("WH-A", reportRows.ElementAt(0).Stock);
            Assert.AreEqual(5, reportRows.ElementAt(0).Materials.Count());

            Assert.AreEqual(33, reportRows.ElementAt(1).Total);
            Assert.AreEqual("WH-C", reportRows.ElementAt(1).Stock);
            Assert.AreEqual(4, reportRows.ElementAt(1).Materials.Count());

            Assert.AreEqual(33, reportRows.ElementAt(2).Total);
            Assert.AreEqual("WH-B", reportRows.ElementAt(2).Stock);
            Assert.AreEqual(5, reportRows.ElementAt(2).Materials.Count());
        }
    }
}
