﻿using System.Collections.Generic;

namespace StockImporter
{
    public class Material
    {
        public string Name { get; set; }
        public string ID { get; set; }
        public string Stock { get; set; }
        public int Amount { get; set; }        
    }
}
