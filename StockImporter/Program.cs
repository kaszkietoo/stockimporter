﻿using System;
using System.Collections.Generic;

namespace StockImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var stockReader = new StockReader();
            var stockParser = new StockParser();
            var stockWriter = new StockWriter();

            var entries = stockReader.Read(Console.In);
            var materials = stockParser.Parse(entries);

            stockWriter.Write(materials, Console.Out);
        }
    }
}
