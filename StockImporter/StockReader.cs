﻿using System.Collections.Generic;
using System.IO;

namespace StockImporter
{
    public class StockReader
    {
        public IEnumerable<string> Read(TextReader reader)
        {
            var linesToParse = new List<string>();
            string line = reader.ReadLine();

            while (!string.IsNullOrWhiteSpace(line))
            {
                if (!line.StartsWith('#'))
                    linesToParse.Add(line);

                line = reader.ReadLine();
            }

            return linesToParse;
        }
    }
}
