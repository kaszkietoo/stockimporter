﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace StockImporter
{
    public class StockWriter
    {
        public void Write(IEnumerable<Material> materials, TextWriter writer)
        {
            var reporter = new StockReporter();
            var reportRows = reporter.GetReport(materials);

            foreach (var row in reportRows)
            {
                writer.WriteLine(string.Format("{0} (total {1})", row.Stock, row.Total));

                foreach (var material in row.Materials.OrderBy(m => m.ID))
                {
                    Console.WriteLine(string.Format("{0}: {1}", material.ID, material.Amount));
                }

                Console.WriteLine();
            }


            
        }
    }
}
