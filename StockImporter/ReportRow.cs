﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockImporter
{
    public class ReportRow
    {
        public string Stock { get; set; }
        public int Total { get; set; }
        public IGrouping<string, Material> Materials { get; set; }
    }
}
