﻿using System.Collections.Generic;
using System.Linq;

namespace StockImporter
{
    public class StockReporter
    {
        public IEnumerable<ReportRow> GetReport(IEnumerable<Material> materials)
        {
            var report = materials
                .GroupBy(m => m.Stock)
                .Select(x => new ReportRow { Stock = x.Key, Total = x.Sum(x1 => x1.Amount), Materials = x })
                .OrderByDescending(x => x.Total)
                .ThenByDescending(x => x.Stock);                

            return report;
        }
    }
}
