﻿using System.Collections.Generic;
using System.Linq;

namespace StockImporter
{
    public class StockParser
    {
        public IEnumerable<Material> Parse(IEnumerable<string> entries)
        {            
            return GetMaterials(entries);
        }
        
        private IEnumerable<string> GetMaterialFactors(string entry)
        {
            return entry.Split(';');
        }

        private IEnumerable<string> GetMaterialQuantities(IEnumerable<string> materialFactors)
        {
            return materialFactors.ElementAt(2).Split('|');
        }       

        private IEnumerable<string> GetStockAndAmount(string quantity)
        {
            return quantity.Split(',');
        }

        private IEnumerable<Material> GetMaterials(IEnumerable<string> entries)
        {
            var materials = new List<Material>();

            foreach (var entry in entries)
            {
                IEnumerable<string> materialFactors = GetMaterialFactors(entry);
                IEnumerable<string> quantities = GetMaterialQuantities(materialFactors);

                foreach (var quantity in quantities)
                {
                    IEnumerable<string> stockAndAmount = GetStockAndAmount(quantity);
                    materials.Add(
                        new Material
                        {
                            Name = materialFactors.ElementAt(0),
                            ID = materialFactors.ElementAt(1),
                            Stock = stockAndAmount.ElementAt(0),
                            Amount = int.Parse(stockAndAmount.ElementAt(1))
                        });
                }
            }

            return materials;
        }
    }
}
